
import UIKit
import CoreMotion
import GLKit
import GCDWebServer
import WebKit



class ViewController: UIViewController
{
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var infoView: UITextView!


    // MARK: - * Constants *
    // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
//    let kTIPPING_POINT = Float(45.0)
    let kTIPPING_POINT = Double(325.0)



    // MARK: - * Variables *
    // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
    let motionManager = CMMotionManager()
//    var pitchAngle = Float()
    var magneticField = Double()

    let webServer = GCDWebServer()



    // MARK: - * lifecycle *
    // ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // web server setup
        let wwwFolderURL = URL.init(fileURLWithPath: Bundle.main.bundleURL.appendingPathComponent("www").path)

        webServer.addGETHandler(forBasePath:"/", directoryPath:wwwFolderURL.path, indexFilename:nil, cacheAge:3600, allowRangeRequests:true)
        webServer.addHandler(forMethod: "GET", path:"/" ,request:GCDWebServerRequest.self, processBlock: {request in

            var htmlFilePath = String()

//            print("\(self.nameOfClass).\(#function): Pitch = \(self.pitchAngle)°")
            print("\(self.nameOfClass).\(#function): magneticField = \(self.magneticField)")

            if (self.isDoorOpen())
            {
                htmlFilePath = wwwFolderURL.appendingPathComponent("garageDoorOpen.html").path
            }
            else
            {
                htmlFilePath = wwwFolderURL.appendingPathComponent("garageDoorClosed.html").path
            }

            self.webView.load(NSURLRequest(url: NSURL(string:htmlFilePath)! as URL) as URLRequest)

            var vars: [AnyHashable: Any]
            vars = [:]
            return GCDWebServerDataResponse(htmlTemplate:htmlFilePath, variables: vars)
        })

        webServer.start(withPort: 8080, bonjourName: "Door Status iPhone Web Server")
        infoView.text = "server URL = \(webServer.serverURL!)"

        self.webView.load(NSURLRequest(url: (webServer.serverURL)!) as URLRequest)
    }



    override func viewDidAppear(_ animated: Bool)
    {
        UIApplication.shared.isIdleTimerDisabled = true // no sleep for you
        UIScreen.main.brightness = CGFloat(0.0)         // dim the screen completely

        if motionManager.isMagnetometerAvailable
        {
            struct door { static var status = Bool(false) } // https://gist.github.com/bryanluby/548beba8f8062c81ed68   (seriously?)
            door.status = isDoorOpen()

            motionManager.magnetometerUpdateInterval = 15.0/60.0;  // frequency (4 Hz)

            func handleMove(magneticData: CMMagnetometerData?, error: Error?)
            {
                if (magneticData != nil)
                {
                    let x = abs(magneticData!.magneticField.x)
                    let y = abs(magneticData!.magneticField.y)
                    let z = abs(magneticData!.magneticField.z)

                    // averge
                    self.magneticField = abs(((x + y + z)/3.0)).rounded()

//                    print("\(self.nameOfClass).\(#function): x:=\(x), y:=\(y), z:=\(z)")
//                    print("\(self.nameOfClass).\(#function): magneticField = \(self.magneticField)")

                    if (door.status != isDoorOpen())    // Only update upon a valid change in state
                    {
                        door.status = isDoorOpen()      // new state
                        print("\(self.nameOfClass).\(#function): magneticField = \(self.magneticField)")
                        webView.load(NSURLRequest(url: (webServer.serverURL)!) as URLRequest)
                    }
                }
            }

            motionManager.startMagnetometerUpdates(to: OperationQueue(), withHandler: handleMove)

/*
        if motionManager.isDeviceMotionAvailable
        {
            struct door { static var status = Bool(false) } // https://gist.github.com/bryanluby/548beba8f8062c81ed68   (seriously?)
            door.status = isDoorOpen()

            motionManager.deviceMotionUpdateInterval = 30.0/60.0;  // frequency

            func handleMove(motionData: CMDeviceMotion?, error: Error?)
            {
                pitchAngle = GLKMathRadiansToDegrees(Float((motionData?.attitude.pitch)!)).rounded()

                if (door.status != isDoorOpen())    // Only update upon a valid change in state
                {
                    door.status = isDoorOpen()      // new state
                    print("\(self.nameOfClass).\(#function): Pitch = \(pitchAngle)°")
                    webView.load(NSURLRequest(url: (webServer.serverURL)!) as URLRequest)
                }
            }

            motionManager.startDeviceMotionUpdates(to: OperationQueue(), withHandler: handleMove)
*/

        }
        else
        {
            print("\(self.nameOfClass).\(#function): Core Motion access denied")
        }
    }



    override func viewWillDisappear(_ animated: Bool)
    {
//        motionManager.stopDeviceMotionUpdates()
        motionManager.stopMagnetometerUpdates();
    }



    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        print("\(self.nameOfClass).\(#function): !!?")
    }



    func isDoorOpen() -> Bool
    {
        return (abs(self.magneticField) > self.kTIPPING_POINT) ? true : false
//        return (abs(self.pitchAngle) < self.kTIPPING_POINT) ? true : false
    }

}
